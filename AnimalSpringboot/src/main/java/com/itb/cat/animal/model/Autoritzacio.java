package com.itb.cat.animal.model;

import javax.persistence.*;

/**
 * Es necessiten 2 taules: 1 per usuaris i 1 per autorització
 */

@Entity
public class Autoritzacio {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column
    private String authority;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}