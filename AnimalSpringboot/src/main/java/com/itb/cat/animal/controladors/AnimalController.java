package com.itb.cat.animal.controladors;

import com.itb.cat.animal.model.Animal;
import com.itb.cat.animal.serveis.ServeiAnimals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AnimalController {

    @Autowired
    private ServeiAnimals serveiAnimals;  //Sense un @Service al ServeiAnimals no funciona

    @GetMapping("/animal/list")
    public String llistar(Model m){
        m.addAttribute("llistaAnimals",serveiAnimals.findAll());
        return "llistat";
    }

    //consulta d'animal estil REST per id
    @GetMapping("/animal/consulta/{id}")
    public String consulta(@PathVariable int id, Model m){
        Animal animal=serveiAnimals.findById(id);
            if(animal!=null) {
                //animal és el nom de l'objecte que passem a la vista
                m.addAttribute("animal", animal);
                return "consulta";
            }
            else return "error";
    }


    @GetMapping("/animal/new")
    public String afegirAnimal(Model m){
        //cal instanciar l'animalleat, pq sino, el CommandObject no existeix al formulari
        m.addAttribute("animalForm",new Animal());  //<h1 th:text="${animalForm.id!=0}?  fa referència a aquest thead del html
        return "afegir";
    }




    @PostMapping("animal/new/submit")
    //empleatForm és el nom de l'objecte que es recull al formulari, el CommandObject (bean)
    //https://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html#handling-the-command-object
    public String afegirSubmit(@ModelAttribute("animalForm") Animal animal){
        serveiAnimals.add(animal);
        return "redirect:/animal/list";
    }


    @PostMapping("animal/edit/submit")
    //animalleatForm és el nom de l'objecte que es recull al formulari, el CommandObject (bean)
    //https://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html#handling-the-command-object
    public String substituirSubmit(@ModelAttribute("animalleatForm") Animal animal){
        serveiAnimals.substituir(animal);
        return "redirect:/animal/list";
    }

    @GetMapping("/animal/edit/{id}")
    public String substituirAnimal(@PathVariable int id, Model m){
        Animal animal=serveiAnimals.findById(id);
        if(animal!=null){
            m.addAttribute("animalleatForm",animal);
            return "afegir";
        }
        else return "redirect:/animal/new";
    }


    @GetMapping("/animal/eliminar/{id}")
    public String eliminarAnimal(@PathVariable int id){
        serveiAnimals.eliminar(id);
        return "redirect:/animal/list";
    }

}
