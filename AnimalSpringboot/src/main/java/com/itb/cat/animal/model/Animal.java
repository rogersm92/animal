package com.itb.cat.animal.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Animal implements Comparable{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String imatgeUrl;
    private String nom;
    private String genere;
    private String personalitat;
    private boolean esGuai;
    private boolean esImprescindible;

    public Animal(String imatgeUrl, String nom, String genere, String personalitat, boolean esGuai, boolean esImprescindible) {
        this.imatgeUrl=imatgeUrl;
        this.nom=nom;
        this.genere = genere;
        this.personalitat = personalitat;
        this.esGuai = esGuai;
        this.esImprescindible = esImprescindible;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
