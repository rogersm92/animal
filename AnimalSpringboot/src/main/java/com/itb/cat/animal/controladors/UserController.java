package com.itb.cat.animal.controladors;


import com.itb.cat.animal.model.Usuari;
import com.itb.cat.animal.serveis.ServeiUsuaris;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller//cal mapejar aquesta classe a la vista del login.html
public class UserController {

    @Autowired
    private ServeiUsuaris servei;

    @GetMapping("/login")  //cal crear la vista/template al directori templates
    public String login(){
        return "login";
    }

    @GetMapping("/registre")
    public String registre(Model m){
        m.addAttribute("usuariForm",new Usuari());
        return "registre";
    }

    @PostMapping("/registre")
    public String registrarUsuari(@ModelAttribute Usuari user) throws Exception{
        servei.registrar(user);
        return "redirect:/index"; //o redirect:  ?? ja que index no està a la urlA
    }

}
