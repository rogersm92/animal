package com.itb.cat.animal.repositoris;

import com.itb.cat.animal.model.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface RepositoriAnimal extends JpaRepository<Animal, Integer> {   //Integer és la clau primària
}
