package com.itb.cat.animal.repositoris;

import com.itb.cat.animal.model.Usuari;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoriUsuari extends JpaRepository<Usuari, String> { //El id és un String i serà clau primària
    Usuari findByUsername(String username);
}
