package com.itb.cat.animal.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Usuari implements Serializable {
    @Id
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String rol; //"USER" o "ADMIN"

    public Usuari(String user, String pwd) {
        username=user;
        password=pwd;
        rol="USER"; //per defecte, tothom és USER
    }

}