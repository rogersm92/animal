package com.itb.cat.animal.serveis;

import com.itb.cat.animal.model.Usuari;
import com.itb.cat.animal.repositoris.RepositoriUsuari;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ServeiUsuaris {
    @Autowired
    private RepositoriUsuari repositori;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder; //sense @Autowired a SecurityAnimal, no funciona

    public void registrar(Usuari u) throws Exception {
        Usuari j = repositori.findByUsername(u.getUsername());
        if (j == null) {
            u.setPassword(passwordEncoder.encode(u.getPassword()));
            repositori.save(u);
        } else throw new Exception("Usuari ja existent");
    }

    public Usuari findById(String username) {
        return repositori.findByUsername(username);
    }


    @PostConstruct
    public void afegirInici() {
        Usuari u1 = new Usuari("Montse", passwordEncoder.encode("1234"), "ADMIN");
        Usuari u2 = new Usuari("Pepi", passwordEncoder.encode("lololo1234"), "ADMIN");
        Usuari u3 = new Usuari("Joan", passwordEncoder.encode("5678"), "USER");
        Usuari u4 = new Usuari("Alberto", passwordEncoder.encode("5678"), "USER");

        repositori.save(u1);
        repositori.save(u2);
        repositori.save(u3);
        repositori.save(u4);
    }
}