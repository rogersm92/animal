package com.itb.cat.animal.serveis;

import com.itb.cat.animal.model.Animal;
import com.itb.cat.animal.repositoris.RepositoriAnimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ServeiAnimals {

    @Autowired
    private RepositoriAnimal repositoriAnimal;

    //Create
    public void add(Animal animal) {

        repositoriAnimal.save(animal);
        //si volem, es pot aprofitar l'objecte que retorna, Animal
    }

    //Read or Retrieve
    public List<Animal> findAll() {
        return repositoriAnimal.findAll();
    }

    @PostConstruct
    //Sempre inicialitza un llistat per tenir alguna cosa
    public void afegirInici() {
        repositoriAnimal.save(new Animal(1, "https://pm1.narvii.com/6279/967f90b0b3bd1a28833234873968bf45433b3653_00.jpg", "Patri","Femení","Presumit/da", true, true));
        repositoriAnimal.save(new Animal(2, "https://vignette.wikia.nocookie.net/animalcrossing/images/6/67/Barold_HD.png/revision/latest/scale-to-width-down/350?cb=20180518161236", "Plácido", "Masculí", "Mandrós/a", false, false));
    }

    //Read or Retrieve
    //buscar per id
    public Animal findById(int id){
        return repositoriAnimal.findById(id).orElse(null);
    }

    //Delete
    //eliminar per id
    //si no existeix l'id no fa res
    public void eliminar(int id){
        repositoriAnimal.deleteById(id);
    }

    //Update
    //substituir empleat
    public void substituir(Animal animal){
        //save, si ja existeix empleat modifica els camps.
        //si no existeix, l'afegeix.
        repositoriAnimal.save(animal);
    }

}
